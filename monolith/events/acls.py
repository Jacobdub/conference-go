from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    headers = {
        "Authorization": PEXELS_API_KEY
    }
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    url = f"https://api.pexels.com/v1/search"
    response = requests.get(url, headers=headers, params=params)
    content = json.loads(response.content)
    photo_data = {"picture_url": content["photos"][0]["src"]["original"]}
    try:
        return photo_data
    except:
        return {"picture_url": None}


def get_weather(city, state):
    payload = {
        "q": f"{city}, {state}, US",
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1,
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=payload)
    content = json.loads(response.content)
    try:
        lat = content[0]["lat"]
        lon = content[0]["lon"]
    except:
        lat = None
        lon = None

    payload2 = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }

    url2 = "https://api.openweathermap.org/data/2.5/weather"
    response2 = requests.get(url2, params=payload2)
    content2 = json.loads(response2.content)

    try:
        temp = content2["main"]["temp"]
        description = content2["weather"][0]["description"]
        return {"temp": temp, "description": description}
    except:
        return {"temp": None, "description": None}

